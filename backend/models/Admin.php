<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * Admin model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $new_password
 * @property string $username
 * @property string $created_at
 * @property string $updated_at
 */
class Admin extends ActiveRecord implements IdentityInterface
{
    const ROLE_ADMIN = 10;
    const ROLE_MANAGER = 0;

    public $new_password;

    public static function roleList()
    {
        return [
            static::ROLE_ADMIN => 'Admin',
            static::ROLE_MANAGER => 'Manager',
        ];
    }

    public function rules()
    {
        return [
            [['username', 'new_password'], 'string'],
            ['role', 'in', 'range' => [static::ROLE_ADMIN, static::ROLE_MANAGER]],
        ];
    }

    public function beforeSave($insert)
    {
        $this->updated_at = new Expression('now()');
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function getAuthKey()
    {
    }

    public function validateAuthKey($authKey)
    {
    }
}