<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.07.2017
 * Time: 20:29
 */

namespace backend\models;


class AdminForm extends Admin
{
    public $new_password;

    public function rules()
    {
        $parent_rules = parent::rules();
        $parent_rules[] = ['new_password', 'string', 'min' => 6];
        return parent::rules();
    }

    public function beforeSave($insert)
    {
        if (!empty($this->new_password)) {
            $this->setPassword($this->new_password);
            $this->new_password = null;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}