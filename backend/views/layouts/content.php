<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

/** @var $content string */

?>
<div class="content-wrapper">
    <section class="content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    Web Admin Panel
</footer>