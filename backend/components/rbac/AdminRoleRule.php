<?php
namespace backend\components\rbac;

use backend\models\Admin;
use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;

class AdminRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {
        $user = ArrayHelper::getValue($params, 'user', Admin::findOne($user));
        if ($user) {
            $role = $user->role;
            if ($item->name === 'admin') {
                return $role == Admin::ROLE_ADMIN;
            } elseif ($item->name === 'manager') {
                return $role == Admin::ROLE_MANAGER;
            }
        }
        return false;
    }
}