<?php
return [
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'ruleName' => 'userRole',
        'children' => [
            'manager',
        ],
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Manager',
        'ruleName' => 'userRole',
    ],
];
