<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use backend\components\rbac\AdminRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $rule = new AdminRoleRule();
        $auth->add($rule);

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        $manager = $auth->createRole('manager');
        $manager->description = 'Manager';
        $manager->ruleName = $rule->name;
        $auth->add($manager);

        $auth->addChild($admin, $manager);
    }
}