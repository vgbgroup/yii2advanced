<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            
            'password_reset_token' => $this->string()->unique(),
            'confirm_token' => $this->string()->unique(),
            
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => 'timestamp without time zone not null default now()',
            'updated_at' => 'timestamp without time zone not null default now()',
        ], $tableOptions);
        
        $this->createTable('admin', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            
            'role' => $this->integer()->notNull()->defaultValue(1),
            
            'created_at' => 'timestamp without time zone not null default now()',
            'updated_at' => 'timestamp without time zone not null default now()',
        ], $tableOptions);
        
        $this->insert('admin', [
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'role' => 10,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
        $this->dropTable('admin');
    }
}
