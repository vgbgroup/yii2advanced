<?php

namespace common\helpers;

use Yii;
use yii\helpers\BaseArrayHelper;

class ArrayHelper extends BaseArrayHelper
{
    public static function flatMap(callable $func, array $array) {
        return static::flat(array_map($func, $array));
    }

    public static function flat(array $array)
    {
        return array_reduce(
            $array,
            'array_merge',
            []
        );
    }

    public static function first($array)
    {
        if (is_array($array)) {
            if (empty($array)) {
                return null;
            }
            return $array[0];
        }
        return $array;
    }

    public static function last($array)
    {
        if (is_array($array)) {
            if (empty($array)) {
                return null;
            }
            return $array[count($array) - 1];
        }
        return $array;
    }

}