<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm', 'token' => $user->confirm_token]);
?>
Hello <?= $user->email ?>,

Follow the link below to confirm your email:

<?= $resetLink ?>
